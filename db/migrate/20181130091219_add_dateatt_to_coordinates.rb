class AddDateattToCoordinates < ActiveRecord::Migration[5.2]
  def change
    add_column :coordinates, :dateatt, :datetime
  end
end
