class CreateCoordinates < ActiveRecord::Migration[5.2]
  def change
    create_table :coordinates do |t|
      t.string :lat
      t.string :long

      t.timestamps
    end
  end
end
