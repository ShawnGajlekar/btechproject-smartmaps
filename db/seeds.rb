# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
aFile = File.open(Rails.root.join('db',"coordinateseed.txt"),"r+")
while (line = aFile.gets)
	coord_lat = line.partition(",").first
	coord_long = line.partition(",").last
	coord = Coordinate.create([{lat: coord_lat,long: coord_long, dateatt: Date.today}])
end