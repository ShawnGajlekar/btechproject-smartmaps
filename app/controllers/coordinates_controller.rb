class CoordinatesController < ApplicationController
	def send_coords
	  date = params[:date_params].to_date
	  coords = Coordinate.where(dateatt: date.all_day)
	  render json: coords
	end

	def send_selected_coords
		@date = params[:date_params].to_date
		@radius = params[:radius].to_i
		@passed_lat = params[:passed_lat].to_f
		@passed_long = params[:passed_long].to_f
		@coords = Coordinate.where(dateatt: @date.all_day)
		@coordinates = Array.new
		@count = 0
		for co in @coords
			loc1 = [@passed_lat,@passed_long]
			loc2 = [co.lat.to_f, co.long.to_f]
			if Coordinate.distance(loc1,loc2) < @radius
				@coordinates.insert(@count,co)
				@count = @count + 1
			end
		end
		render json: @coordinates
	end

end
