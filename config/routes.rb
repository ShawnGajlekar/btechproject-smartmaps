Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :coordinates
  get 'send_coords', to: 'coordinates#send_coords'
  get 'send_selected_coords',to: 'coordinates#send_selected_coords'
end
